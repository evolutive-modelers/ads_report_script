import pandas as pd
import os
import datetime
import socket
from sqlalchemy import create_engine
import csv
import tqdm  
import math
import re
import time
from dateutil import parser
import gspread
from oauth2client.service_account import ServiceAccountCredentials
import mysql.connector

website_list = ["'amazon.com'", "'amazon.it'", "'amazon.fr'", "'amazon.es'", "'amazon.de'", "'amazon.uk'"]
region = ["US", "IT", "FR", "ES", "DE", "UK"]
currency = ["USD", "EUR"]

#計算amazon廣告損益
amazon_db = mysql.connector.connect(
  host = "amz-mkp.ck4fc6pfwu0d.us-west-2.rds.amazonaws.com",
  user = "report",
  password = "7VD#o@Leh*QMBb",
  database = "marketplace",
  )
cursor=amazon_db.cursor()

query = "SELECT * FROM advertising_reports where record_type LIKE '%Campaign%'" 
cursor.execute(query)
advertising_reports = cursor.fetchall()
num_fields = len(cursor.description)
column_names = [i[0] for i in cursor.description]
advertising_reports = pd.DataFrame(advertising_reports,columns=column_names)

advertising_report_sum = advertising_reports.groupby(["report_date"],as_index = False)[["cost", "attributedSales1d"]].sum()

advertising_report_sum["website"] = region[0]
advertising_report_sum["currency"] = currency[0]

query = "SELECT * FROM business_reports where website = " + website_list[0]
cursor.execute(query)
business_data = cursor.fetchall()
num_fields = len(cursor.description)
column_names = [i[0] for i in cursor.description]
business_reports = pd.DataFrame(business_data,columns=column_names)

business_reports["ordered_product_sales"] = business_reports['ordered_product_sales'].str.split('$', 1).str[1]
business_reports['ordered_product_sales'] = business_reports['ordered_product_sales'].str.replace(',','')
business_reports['ordered_product_sales'] = business_reports['ordered_product_sales'].astype('float64')

business_reports_sum = business_reports.groupby(["report_date"],as_index = False)[["ordered_product_sales"]].sum()
business_advertising_merge = business_reports_sum.merge(advertising_report_sum, on = 'report_date',how = 'left')
business_advertising_merge.columns = ["report_date", "total_revenue", "ad_cost", "ad_revenue", "website", "currency"]
business_advertising_merge["ad_rate"] = business_advertising_merge["ad_revenue"] / business_advertising_merge["total_revenue"]
business_advertising_merge["org_revenue"] = business_advertising_merge["total_revenue"] - business_advertising_merge["ad_revenue"]

business_advertising_merge.to_csv("C:/Users/miles/OneDrive - 愛進化科技股份有限公司 (2)/Data/Amazon/report_data/business_advertising_income.csv", header='column_names', index=False, encoding='utf_8_sig')


#連接google_sheet以取得amazon黑科技花費
gc = gspread.service_account(filename='C:/Users/miles/OneDrive - 愛進化科技股份有限公司 (2)/Data/Amazon/report_data/google_sheet_token.json')
gsheet = gc.open_by_url("https://docs.google.com/spreadsheets/d/1IdoBU11ObdRTtSRJH_uXlakESMWaascm_hGmGvDfht8/edit#gid=0")

amazon_black_technology = gsheet.sheet1.get_all_records()
amazon_black_technology_df = pd.DataFrame(amazon_black_technology)
# amazon_black_technology_df['Price'] = amazon_black_technology_df['Price'].str.replace(',','')
# amazon_black_technology_df['Price'] = amazon_black_technology_df['Price'].astype('int64')

amazon_black_technology_df.to_csv("C:/Users/miles/OneDrive - 愛進化科技股份有限公司 (2)/Data/Amazon/report_data/amazon_black_technology_spent.csv", header='column_names', index=False, encoding='utf_8_sig')

#amazon小紅旗計算
amazon_notifications_json = pd.read_json('C:/Users/miles/OneDrive - 愛進化科技股份有限公司 (2)/Data/Amazon/performance-notification/amazon.com.json')
amazon_notifications = pd.json_normalize(amazon_notifications_json["data"])

amazon_notifications['Date'] = pd.to_datetime(amazon_notifications['content.created'], unit='ms').dt.date
notifications_all = amazon_notifications[["content.subject", "Date"]]

notifications_filter = amazon_notifications[amazon_notifications['content.subject'].str.contains('Action required: Your listing of')]

notifications_filter["content.subject_split"] = notifications_filter['content.subject'].str.split('Action required: Your listing of ', 1).str[1]
notifications_filter["content.subject_split_2"] = notifications_filter['content.subject_split'].str.split(' has been', 1).str[0]
notifications_filter["content.subject_sku"] = notifications_filter['content.subject_split_2'].str.split(',', 1).str[0]

notifications = notifications_filter[["content.subject", "content.subject_sku", "Date"]]
notifications["website"] = region[0]

notifications_sku_count = notifications.groupby(['content.subject_sku']).size().reset_index(name='counts')
notifications.to_csv("C:/Users/miles/OneDrive - 愛進化科技股份有限公司 (2)/Data/Amazon/report_data/notifications_report.csv", header='column_names', index=False, encoding='utf_8_sig')
notifications_sku_count.to_csv("C:/Users/miles/OneDrive - 愛進化科技股份有限公司 (2)/Data/Amazon/report_data/notifications_sku_count.csv", header='column_names', index=False, encoding='utf_8_sig')
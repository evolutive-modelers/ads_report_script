import json
import pandas as pd
import datetime
from datetime import date, timedelta

def to_csv_append(df, file_name):
    filepath  = ("D:/amazon/" + file_name)
    if not os.path.isfile(filepath):
        df.to_csv(filepath, header='column_names', index=False)
    else:
        df.to_csv(filepath, mode='a', header=False, index=False)

yesterday = date.today() - timedelta(days=1)
yesterday = yesterday.strftime('%Y-%m-%d')

import mysql.connector
amazon_db = mysql.connector.connect(
  host = "amz-mkp.ck4fc6pfwu0d.us-west-2.rds.amazonaws.com",
  user = "report",
  password = "7VD#o@Leh*QMBb",
  database = "marketplace",
  )
cursor=amazon_db.cursor()

cursor.execute("SELECT * FROM campaigns")
campaigns = cursor.fetchall()
num_fields = len(cursor.description)
column_names = [i[0] for i in cursor.description]
campaigns_df = pd.DataFrame(campaigns,columns=column_names)
campaigns = campaigns_df[["id", "name", "campaignId", "dailyBudget"]]

cursor.execute("SELECT * FROM advertising_reports")
advertising_reports = cursor.fetchall()
num_fields = len(cursor.description)
column_names = [i[0] for i in cursor.description]
advertising_reports_df = pd.DataFrame(advertising_reports,columns=column_names)

filter_advertising_df = advertising_reports_df[advertising_reports_df["record_type"].str.contains("Campaign")]
filter_advertising_df['Date'] = filter_advertising_df['updated_at'].apply(lambda x: x.strftime('%Y-%m-%d'))
filter_advertising_df = filter_advertising_df[filter_advertising_df["Date"] == yesterday]
filter_advertising = filter_advertising_df[["record_id", "impressions", "clicks", "cost", "attributedConversions30d", "attributedSales30d", "Date"]]

campaigns_map = pd.merge(campaigns, filter_advertising, how='left', left_on='id', right_on='record_id',)
campaigns_map = campaigns_map.drop_duplicates()
campaigns_value = campaigns_map.dropna()
campaigns_value = campaigns_value[["Date", "name", "campaignId", "dailyBudget", "impressions", "clicks", "cost", "attributedConversions30d", "attributedSales30d"]].reset_index()
campaigns_value["ROAS"] = campaigns_value["attributedSales30d"]/campaigns_value["cost"]
campaigns_value["CTR"] = campaigns_value["clicks"] / campaigns_value["impressions"]
campaigns_value["CVR"] = campaigns_value["attributedConversions30d"] / campaigns_value["clicks"]
campaigns_value["CPC"] = campaigns_value["clicks"] / campaigns_value["cost"]
amazon_campaigns_report = campaigns_value.fillna(0)

to_csv_append(amazon_campaigns_report, "amazon_campaigns_report.csv")

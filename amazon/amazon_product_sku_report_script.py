import pandas as pd
import os
import datetime
import socket
from sqlalchemy import create_engine
import csv
import numpy as np
import pandas as pd
import tqdm  # progress bar
import math
import re
import time
import datetime
from dateutil import parser
import mysql.connector

website_list = ["'amazon.com'", "'amazon.it'", "'amazon.fr'", "'amazon.es'", "'amazon.de'", "'amazon.uk'"]
region = ["US", "IT", "FR", "ES", "DE", "UK"]
currency = ["USD", "EUR"]

amazon_db = mysql.connector.connect(
  host = "amz-mkp.ck4fc6pfwu0d.us-west-2.rds.amazonaws.com",
  user = "report",
  password = "7VD#o@Leh*QMBb",
  database = "marketplace",
  )
cursor=amazon_db.cursor()

for i in range(len(website_list)):
    if i == 0:
        query = "SELECT * FROM business_reports where website = " + website_list[i]
        cursor.execute(query)
        business_data = cursor.fetchall()
        num_fields = len(cursor.description)
        column_names = [i[0] for i in cursor.description]
        business_reports = pd.DataFrame(business_data,columns=column_names)

        sku_business_report = business_reports[["report_date" ,"asin_child", "sku", "title", "sessions", "page_views", 
                                                "units_ordered", "units_ordered_b2b", "unit_session_percentage",
                                                "ordered_product_sales", "ordered_product_sales_b2b", 
                                                "total_order_items", "total_order_items_b2b", "buy_box_percentage"]]

        sku_business_report['Year'] = sku_business_report['report_date'].apply(lambda x: x.strftime('%Y'))
        sku_business_report_2020 = sku_business_report[sku_business_report["Year"] == '2020']
        sku_business_report_2020['unit_session_percentage'] = sku_business_report_2020['unit_session_percentage'].astype(str) + '%'
        sku_business_report_2020['buy_box_percentage'] = sku_business_report_2020['buy_box_percentage'].astype(str) + '%'
        sku_business_report_2020["website"] = region[i]
        sku_business_report_2020["currency"] = currency[0]
        
    else:
        query = "SELECT * FROM business_reports where website = " + website_list[i]
        cursor.execute(query)
        business_data = cursor.fetchall()
        num_fields = len(cursor.description)
        column_names = [i[0] for i in cursor.description]
        business_reports = pd.DataFrame(business_data,columns=column_names)

        sub_sku_business_reports = business_reports[["report_date" ,"asin_child", "sku", "title", "sessions", "page_views", 
                                                     "units_ordered", "units_ordered_b2b", "unit_session_percentage",
                                                     "ordered_product_sales", "ordered_product_sales_b2b", 
                                                     "total_order_items", "total_order_items_b2b", "buy_box_percentage"]]

        sub_sku_business_reports['Year'] = sub_sku_business_reports['report_date'].apply(lambda x: x.strftime('%Y'))
        sub_sku_business_report_2020 = sub_sku_business_reports[sub_sku_business_reports["Year"] == '2020']
        sub_sku_business_report_2020['unit_session_percentage'] = sub_sku_business_report_2020['unit_session_percentage'].astype(str) + '%'
        sub_sku_business_report_2020['buy_box_percentage'] = sub_sku_business_report_2020['buy_box_percentage'].astype(str) + '%'
        sub_sku_business_report_2020["website"] = region[i]
        sub_sku_business_report_2020["currency"] = currency[1]
        
        sku_business_report_2020 = sku_business_report_2020.append(sub_sku_business_report_2020, ignore_index = True) 

sku_business_report_2020 = sku_business_report_2020.fillna(0)
sku_business_report_2020 = sku_business_report_2020.rename(columns={'sku': 'sku_org'})
sku_business_report_2020['sku'] = sku_business_report_2020['sku_org'].str.split('-', 1).str[1]
sku_business_df = sku_business_report_2020
sku_business_report_2020_split = sku_business_report_2020['sku'].str.split('-', expand=True) 
sku_business_df['sku1'] = sku_business_report_2020_split.loc[:, 0]
sku_business_df['sku2'] = sku_business_report_2020_split.loc[:, 1]
sku_business_df['sku1'] = sku_business_df['sku1'].str.lstrip()  
sku_business_df['sku2'] = sku_business_df['sku2'].str.lstrip() 

mpn = pd.read_csv("C:/Users/miles/OneDrive - 愛進化科技股份有限公司 (2)/Data/Grand_dashboard/mpn.csv", header = 0, encoding='Big5')

sku_business_df['sku1'] = sku_business_df['sku1'].str.lstrip()
sku_business_df["sku1"] = sku_business_df["sku1"].replace([np.inf, -np.inf], r'') 
sku_business_df["sku1"] = sku_business_df["sku1"].replace(np.nan, r'')
idx = ['star', 'dsc', 'test', 'TEST', 'sticker', 'ample']
filter = sku_business_df['sku1'].str.contains('|'.join(idx))
sku_business_df_1 = sku_business_df[-filter]
sku_business_df_1['sku_num'] = sku_business_df_1['sku1'].replace(to_replace=r'3PB', regex=True, value=r'')
sku_business_df_1['sku_num'] = sku_business_df_1['sku_num'].replace(to_replace=r'\D', regex=True, value=r'')
sku_business_df_1['ndigit'] = sku_business_df_1['sku_num'].str.len()

sku_business_df_3PB = sku_business_df_1.loc[sku_business_df_1['sku1'].str.contains('3PB')]

sku_business_df_2 = sku_business_df_1[-(sku_business_df_1['ndigit'].isin([0, 8, 12, 13]))]
sku_business_df_2['sku_head'] = sku_business_df_2['sku1'].str.extract(r'(^[A-Za-z]+)')
filter = sku_business_df_2['sku1'].str.contains('3PB', regex=True)
sku_business_df_2.loc[filter, 'sku_head'] = '3PB'
filter_MANX = sku_business_df_2['sku1'].str.contains('MA|NX', regex=True)
sku_business_df_2['sku_head'][filter_MANX] = sku_business_df_2['sku1'][filter_MANX].str.slice(start=0, stop=2) 

sku_business_df_2['sku_tail_id'] = sku_business_df_2['sku1'].str.extract(r'([A-Za-z]$)')
sku_business_df_2['brand_id'] = sku_business_df_2['sku_num'].str.slice(start=0, stop=2)
filter = sku_business_df_2['sku1'].str.contains('EC01K|NX01K', regex=True)
sku_business_df_2['brand_id'][filter] = sku_business_df_2['sku_num'][filter]
filter = sku_business_df_2['sku1'].str.contains('EC050K|NX050K', regex=True)
sku_business_df_2['brand_id'][filter] = '02'

sku_business_df_2['model_id'] = sku_business_df_2['sku_num'].str.slice(start=2, stop=5)
filter = sku_business_df_2['sku1'].str.contains('EC050K|NX050K', regex=True)
sku_business_df_2['model_id'][filter] = '072'

sku_business_df_2['style_id'] = sku_business_df_2['sku1'].str.extract(r'(\w\w$)')
filter = pd.notna(sku_business_df_2.sku_tail_id)
sku_business_df_2['style_id'][filter] = sku_business_df_2['sku1'][filter].str[-3:-1]
filter_1 = sku_business_df_2['sku1'].str.contains('MA', regex=True)
sku_business_df_2['style_id'][filter_1] = sku_business_df_2['sku1'][filter_1].str[2:]

filter_1 = sku_business_df_2['ndigit'] == 4
filter_2 = sku_business_df_2['sku1'].str.contains('EC70K|NX70K|NXA5K', regex=True)
filter_3 = filter_1 & -filter_2
sku_business_df_2['style_id'][filter_3] = sku_business_df_2['sku_num'][filter_3].str[2:4]

filter_1 = sku_business_df_2['ndigit'] == 2
filter_2 = sku_business_df_2['sku1'].str.contains('EC01K', regex=True)
filter_3 = filter_1 & -filter_2
sku_business_df_2['style_id'][filter_3] = sku_business_df_2['sku_num'][filter_3]

filter = sku_business_df_2['sku1'].str.contains('EC70K|NX70K|NXA5K', regex=True)
sku_business_df_2['style_id'][filter] = sku_business_df_2['sku1'][filter].str[2:4]

df_mpn_join_1 = mpn.loc[mpn['category'] == 'sku_head', ['code', 'Type', 'name']] 
df_mpn_join_2 = mpn.loc[mpn['category'] == 'brand', ['code', 'name']]  
df_mpn_join_3 = mpn.loc[mpn['category'] == 'model', ['code', 'name']]  
df_mpn_join_4 = mpn.loc[mpn['category'] == 'style', ['code', 'name']]  
df_mpn_join_5 = mpn.loc[mpn['category'] == 'sku_tail', ['code', 'name']]  

sku_report = sku_business_df_2.merge(df_mpn_join_1, left_on='sku_head', right_on='code', how='left')
sku_report = sku_report.rename(columns={'name': 'product'}) 

sku_report = sku_report.merge(df_mpn_join_2, left_on='brand_id', right_on='code', how='left')
sku_report = sku_report.rename(columns={'name': 'brand'})
sku_report = sku_report.drop(columns=['code_x', 'code_y'])

sku_report = sku_report.merge(df_mpn_join_3, left_on='model_id', right_on='code', how='left')
sku_report = sku_report.rename(columns={'name': 'model'})
sku_report = sku_report.drop(columns=['code'])

sku_report = sku_report.merge(df_mpn_join_4, left_on='style_id', right_on='code', how='left')
sku_report = sku_report.rename(columns={'name': 'style'})
sku_report = sku_report.drop(columns=['code'])

sku_report = sku_report.merge(df_mpn_join_5, left_on='sku_tail_id', right_on='code', how='left')
sku_report = sku_report.rename(columns={'name': 'sku_tail'})
sku_report = sku_report.drop(columns=['code'])

product_sku_report = sku_report[["report_date" ,"asin_child", "sku_org", "sku", "title", "Type", 
                                 "product", "brand", "model", "style", "sessions", "page_views", 
                                 "units_ordered", "units_ordered_b2b", "unit_session_percentage",
                                 "ordered_product_sales", "ordered_product_sales_b2b", "total_order_items", 
                                 "total_order_items_b2b", "buy_box_percentage", "website", "currency"]]

product_sku_report_USD = product_sku_report[product_sku_report["currency"] == "USD"]
product_sku_report_EUR = product_sku_report[product_sku_report["currency"] == "EUR"]
product_sku_report_USD["ordered_product_sales"] = product_sku_report_USD['ordered_product_sales'].str.split('$', 1).str[1]
product_sku_report_USD["ordered_product_sales_b2b"] = product_sku_report_USD['ordered_product_sales_b2b'].str.split('$', 1).str[1]
product_sku_report_EUR["ordered_product_sales"] = product_sku_report_EUR['ordered_product_sales'].str.split('£', 1).str[1]
product_sku_report_all = product_sku_report_USD.append(product_sku_report_EUR, ignore_index = True) 

product_sku_report_all.to_csv("C:/Users/miles/OneDrive - 愛進化科技股份有限公司 (2)/Data/Amazon/report_data/product_sku_report_2020.csv", header='column_names', index=False, encoding='utf_8_sig')
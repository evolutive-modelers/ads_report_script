import pandas as pd
import os
import numpy as np
from pandas.io.json import json_normalize
from facebook_business.api import FacebookAdsApi
from facebook_business.adobjects.adaccount import AdAccount
from facebook_business.adobjects.adaccountuser import AdAccountUser
from facebook_business.adobjects.campaign import Campaign as AdCampaign
from facebook_business.adobjects.adsinsights import AdsInsights

def take_dic_value(df, process_col, process_key, filter_string, new_col_name):
    explode_df = df.explode(process_col).reset_index()
    explode_df = explode_df.dropna()
    filter_df = explode_df[explode_df[process_col].apply(lambda x: x[process_key] == filter_string)]
    filter_df = filter_df.reset_index()
    if len(filter_df) > 0:
        new_df = json_normalize(filter_df[process_col]).filter(like='value')
        new_df.columns = new_df.columns.str.replace('.value','')
        new_df.columns = [new_col_name]
        filter_df = filter_df.join(new_df) 
    else:
        filter_df
        
    return filter_df


def change_types(df):
    convert_dict = {"impressions" : int, "inline_link_clicks" : int, "spend": float, 
                    "website_purchase" : int, "website_purchases_conversion_value" : float}
    df = df.astype(convert_dict)
    return df


def to_csv_append(df, file_name):
    filepath  = ("C:/Users/miles/OneDrive - 愛進化科技股份有限公司 (2)/Data/Ads_platform_report/" + file_name)
    if not os.path.isfile(filepath):
        df.to_csv(filepath, header='column_names', index=False)
    else:
        df.to_csv(filepath, mode='a', header=False, index=False)


def France_region_determination(x):
    if str(x) == "FR ":
        return "France"
    elif str(x) == "DE ":
        return "Germany"
    else:
        return "Spain"

Date = "2020-11-09"

ad_app_list = [["877192079355060", "37df2da5587093f7f0e2f61a49976e4e", "EAAMdzTJKLLQBABk5mYhizTBOZAsX1EtmJZBbz5qRrdGb66itw0vDpuNEOWljEbMyOuTwJPPkHMh0XMESQbIQzAHnTHDdzyFG2wZCh7twoXEZAG9LXZBYZC4noR532zZAFRTzotd2NwP0w76559mxaTFqpMOdjVjJoaDidagvs14WbCyOnEtpQQQ3ABR8VeIASD3dwftb7kZA3eiGzlBE1jH9", "28945450", "Worldwide"],
               ["316009306278479", "02f20516abc8c08f32da5ab895f47e2d", "EAAEfaKZAySk8BAPx87AuaYVTWDBHh88zM0mNyVNTVIxfEm14JJaSks4nT5YDedmVS3kK3junexBmTHmg8uMMMHKJqlVPCtnUCLsZAWCHhaYihG3ixBJZBZCVqpMXuYZAhiwtLsv63dcZAZCY9HNUcozAbolzRtofNjZAIT3gSrtxNAWtT0pPpwwctZAxMmPk1tR8ki7kndezK7B88wUgia58M", "2956186747755778", "France"],
               ["640584500206952", "1c1f1ee995ef9b23d1ce7761f05b0366", "EAAJGm7OVXWgBAFlOPT8Tq2PtmAoA6CFzOEtstfy5Sx0X4RLqdWRtZA23OUaZAFHklEnN2kZB6jJCoEQnZARBmKrD2K7xlo1A9a8ZAiIEhAN7Qg3UeWgC13mgBXDX9EanOifHI1dlEPw4AWEF7LnuPIBWZCpTQJCZAaONTJSr3GZC0v8ymXHiNSMq4NI80pHDNJ2xs0we2vAHzglwcLEqyZCYd", "2357460584492701", "Kroma_France"],
               ["307440447198665", "84075cee4c1338301478de5d25c677f1", "EAAEXnY6lFckBAAqZCSKzLqEWlsJ9ZA06EEi08ZA6E2ZAZCpUwp5y5pOPkYZA9fwxZCxBRA2t7VwtPgrruvSRIp0QmBPFff9fd7rcPxzvqUlFV8ZBRszj19aVGtNXYlIXyhpNl6VyK2HAnUZBjg5SYbCAy37j5pNNmZCZCaGWlrtI1OGZByOq1ZAOus6g0o1T35MtGHrsFhFZCJtZALuO6sAz4UPxQr8", "149577529000023", "Kroma_IO"],
               ["742696043194363", "3c4845e267320278e85a8e7c6503f6b8", "EAAKjemXUpZCsBAOBsqdSccFz8pQUKrqy8REn9RzZAgT6cFgj5TXispisnn0v9JTHZBMFZBKq8tyG1ZCC2oNDcm1b9jTvyOpcSAfw1iQztRYzQNzUzGVuhU9kN53CyXgLUCKuT3NpvBOv0lQmaqSMLgniQ47JgJvPY7QfGQUAlrU2h5gcdNeJhoyGHKiMcZBT5xiqC0xlP88b3uyIrVUtGZA", "96254400", "Taiwan"],
               ["697549350844260", "d8645ec68374e72fdb1ab8f3cbf93c42", "EAAJ6at1FN2QBALZBvuyF6vy2YCacF5zrrhoA461le9FbM89ZCtM7EbPhMNIsQYK7PUKSTaSANm0to881QZAloC86H01HDwHBtEimBsKIyO1N5XlW1UnVOMMtZB5d0b7VxOl48Wfz6sQqkBIgM7GYkEeuyTvfCpJJur3whjk3FQbJJGEK3BfhbnNPTnge7Gz4YpZBJkH2I88HmdFXPWH1i", "192395238714962", "Japan"]]

for i in range(len(ad_app_list)):
    app_id = ad_app_list[i][0]
    app_secret = ad_app_list[i][1]
    access_token = ad_app_list[i][2]
    FacebookAdsApi.init(app_id, app_secret, access_token)
    
    me = AdAccountUser(fbid='me')
    my_accounts = list(me.get_ad_accounts())
    my_ad_accounts = [n for n in my_accounts if n["account_id"] == ad_app_list[i][3]]
    
    params={'time_range': {'since': Date,'until': Date},'level': 'campaign', 'limit': '20000', 'filtering': '[{"field":"ad.effective_status","operator":"IN","value":["ACTIVE"]}]', 'action_attribution_windows': ['1d_view', '28d_click']}
    fields = [AdsInsights.Field.account_id,
              AdsInsights.Field.account_name,
              AdsInsights.Field.campaign_id,
              AdsInsights.Field.campaign_name,
              AdsInsights.Field.spend,
              AdsInsights.Field.impressions,
              AdsInsights.Field.clicks,
              AdsInsights.Field.inline_link_clicks,
              AdsInsights.Field.unique_actions,
              AdsInsights.Field.action_values,
              AdsInsights.Field.reach,
            ]

    for account in my_ad_accounts:
        ads = account.get_insights(params=params, fields=fields)
        print(ads)
        print(len(ads))

    campaign_list = list(ads)
    campaign_data = pd.DataFrame(campaign_list)
    USD_list = ["Taiwan", "Worldwide", "Kroma_IO"]
    JPY_list = ["Japan"]
   
    print(i)
    
    filter_purchase = take_dic_value(campaign_data, "unique_actions", "action_type", "offsite_conversion.fb_pixel_purchase", "website_purchase")
    filter_action_values = take_dic_value(campaign_data, "action_values", "action_type", "offsite_conversion.fb_pixel_purchase", "website_purchases_conversion_value")
    
    if len(filter_purchase) > 0 and len(filter_action_values) > 0:
        filter_purchase_map = filter_purchase[["campaign_name", "website_purchase"]]
        filter_action_values_map = filter_action_values[["campaign_name", "website_purchases_conversion_value"]]
        campaign_report = campaign_data.merge(filter_purchase_map, on = 'campaign_name',how = 'left')
        campaign_report = campaign_report.merge(filter_action_values_map, on = 'campaign_name',how = 'left')
        campaign_report = campaign_report.fillna(0)
        campaign_report_data = campaign_report[["campaign_name", "date_stop", "impressions", "inline_link_clicks", 
                                                "spend", "website_purchase", "website_purchases_conversion_value"]]

        campaign_report_data = change_types(campaign_report_data)
        
        if ad_app_list[i][4] == "France":
            campaign_report_data[["split_col1", "split_col2"]] = campaign_report_data['campaign_name'].str.split('-', expand = True,n=1)
            campaign_report_data["market"] = campaign_report_data["split_col1"].apply(France_region_determination)        
        
        else:
            campaign_report_data["market"] = ad_app_list[i][4]
        
        campaign_report_data_sum = campaign_report_data.groupby(["market", "date_stop"])[["impressions", "spend", "inline_link_clicks", "website_purchase", "website_purchases_conversion_value"]].sum()
        
        campaign_report_data_sum["CTR"] = campaign_report_data_sum["inline_link_clicks"] / campaign_report_data_sum["impressions"]
        campaign_report_data_sum["CVR"] = campaign_report_data_sum["website_purchase"] / campaign_report_data_sum["inline_link_clicks"]
        campaign_report_data_sum["ROAS"] = campaign_report_data_sum["website_purchases_conversion_value"] / campaign_report_data_sum["spend"]
        campaign_report_data_sum["CPC"] = campaign_report_data_sum["inline_link_clicks"] / campaign_report_data_sum["spend"]
        
        if ad_app_list[i][4] in USD_list:
            campaign_report_data_sum["currency"] = "USD"
        elif ad_app_list[i][4] in JPY_list:
            campaign_report_data_sum["currency"] = "JPY"
        else:
            campaign_report_data_sum["currency"] = "EUR"

        campaign_report_data_sum["Platform"] = "Facebook"
         
        campaign_report_data_sum = campaign_report_data_sum.reset_index()
        campaign_report_data_sum = campaign_report_data_sum[["Platform", "date_stop", "market", "impressions", "inline_link_clicks", 
                                                             "spend", "website_purchase", "website_purchases_conversion_value", "CTR",
                                                             "CVR", "ROAS", "CPC", "currency"]]
        campaign_report_data_sum.columns = ["Platform", "Date", "Market", "Impressions", "Clicks", "Costs", "Purchases", 
                                            "Conversion_value", "CTR", "CVR", "ROAS", "CPC", "Currency"]
        
        to_csv_append(campaign_report_data_sum, "FB_ads_insight_report.csv")

    else:
        
        campaign_report = campaign_data.fillna(0)
        campaign_report_data = campaign_report[["campaign_name", "date_stop", "impressions", "inline_link_clicks", "spend"]]
        
        convert_dict = {"impressions" : int, "inline_link_clicks" : int, "spend": float}
        campaign_report_data = campaign_report_data.astype(convert_dict)
        
        if ad_app_list[i][4] ==  "France":
            campaign_report_data[["split_col1", "split_col2"]] = campaign_report_data['campaign_name'].str.split('-', expand = True,n=1)
            campaign_report_data["market"] = campaign_report_data["split_col1"].apply(France_region_determination)
        else:
            campaign_report_data["market"] = ad_app_list[i][4]
            
        campaign_report_data_sum = campaign_report_data.groupby(["market", "date_stop"])[["impressions", "spend", "inline_link_clicks", "website_purchase", "website_purchases_conversion_value"]].sum()
        

        campaign_report_data_sum["website_purchase"] = 0

        campaign_report_data_sum["website_purchases_conversion_value"] = 0

        campaign_report_data_sum["website_purchase_roas_value"] = 0

        if ad_app_list[i][4] in USD_list:
            campaign_report_data_sum["currency"] = "USD"
        elif ad_app_list[i][4] in JPY_list:
            campaign_report_data_sum["currency"] = "JPY"
        else:
            campaign_report_data_sum["currency"] = "EUR"

        campaign_report_data_sum["Platform"] = "Facebook"

        campaign_report_data_sum = campaign_report_data_sum.reset_index()
        campaign_report_data_sum = campaign_report_data_sum[["Platform", "date_stop", "market", "impressions", "inline_link_clicks", 
        													 "spend", "website_purchase", "website_purchases_conversion_value", "CTR",
                                                             "CVR", "ROAS", "CPC", "currency"]]
        
        campaign_report_data_sum.columns = ["Platform", "Date", "Market", "Impressions", "Clicks", "Costs", "Purchases", 
        									"Conversion_value", "CTR", "CVR", "ROAS", "CPC", "Currency"]
        
        to_csv_append(campaign_report_data_sum, "FB_ads_insight_report.csv")
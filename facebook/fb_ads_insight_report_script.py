import pandas as pd
import os
from datetime import date, timedelta
from pandas.io.json import json_normalize
from facebook_business.api import FacebookAdsApi
from facebook_business.adobjects.adaccountuser import AdAccountUser
from facebook_business.adobjects.adsinsights import AdsInsights

day_before_yesterday = date.today() - timedelta(days=2)
day_before_yesterday = day_before_yesterday.strftime('%Y-%m-%d')

def take_dic_value(df, process_col, process_key, filter_string, new_col_name):
    explode_df = df.explode(process_col).reset_index()
    explode_df = explode_df.dropna()
    filter_df = explode_df[explode_df[process_col].apply(lambda x: x[process_key] == filter_string)]
    filter_df = filter_df.reset_index()
    if len(filter_df) > 0:
        new_df = json_normalize(filter_df[process_col]).filter(like='value')
        new_df.columns = new_df.columns.str.replace('.value','')
        new_df.columns = [new_col_name]
        filter_df = filter_df.join(new_df) 
    else:
        filter_df
        
    return filter_df


def change_types(df):
    convert_dict = {"impressions" : int, "inline_link_clicks" : int, "spend": float, 
                    "website_purchase" : int, "website_purchases_conversion_value" : float}
    df = df.astype(convert_dict)
    return df


def to_csv_append(df, file_name):
    filepath  = ("C:/Users/miles/OneDrive - 愛進化科技股份有限公司 (2)/Data/Ads_platform_report/" + file_name)
    if not os.path.isfile(filepath):
        df.to_csv(filepath, header='column_names', index=False)
    else:
        df.to_csv(filepath, mode='a', header=False, index=False)


def France_region_determination(x):
    if str(x) == "FR ":
        return "France"
    elif str(x) == "DE ":
        return "Germany"
    else:
        return "Spain"

ad_app_list = [["877192079355060", "37df2da5587093f7f0e2f61a49976e4e", "EAAMdzTJKLLQBAHwRE65FDTedtPeJZBPfZBRWWowc8jRTzJPT2oaIkChcYxukcEIRudgxpm5TetmMP2Axd848eifZAaBbontiw6de92kQzUiHt4shJiy3rUowXsmiIYbZBUAD3RTziJ8C6JXZAjOEy6KsE98ZAwospwukthaTa3zEVTZAj0RpKalbdr6sZCeMptoZD", "28945450", "Worldwide"],
               ["316009306278479", "02f20516abc8c08f32da5ab895f47e2d", "EAAEfaKZAySk8BAGC4Cr7Ih6euwgb7rMvpoFIdSaWDeFfwo5fZAZAri7Maqx8m39cbNZAvHIX3DgeNZAZBxwJL7KzB90mBm4m3L7nJ5mXsj670ZAbki58szod0r7m5lPnc4nrhqvcpyW4s5RsIkmGKuCRtcMrwbrL5px1wGrKIuZBQ1nbEZAZBX02CCq9C5gSPQzZCcZD", "2956186747755778", "France"],
               ["640584500206952", "1c1f1ee995ef9b23d1ce7761f05b0366", "EAAJGm7OVXWgBAB7ycQFKpRnmBki2BLrre3RsK5ZA4cpuxWZBV4c1xik9aKPuiDpgnRK2xiAepgwq905VhooIUehZBIYf2lEYCg2oNF4z8ZBYPpp9wOMutBIENMAqHvtEKklZAYjHkEV1HZB0cZAsbKsBoZCQ3E165D4yNw73BaR8yrnqKI2gXWLRqsQ73y2tdLEZD", "2357460584492701", "Kroma_France"],
               ["307440447198665", "84075cee4c1338301478de5d25c677f1", "EAAEXnY6lFckBAPObktsq3Iuca0J0MgFsK0tYwXfdZACM9vO2Hw3Yt2j9bsu06VUObVfKdp1VPVK6HfvH8DMk5ZBQfkbjeZBmjX2kR32kGJIZAOSI8PLiTVlzzOCXfJ0dZARsXJFQ5PHJ8qhY7J27g8oL9ovc07v2ywPMISFydihL1hyZCYzpWN7524PzrEvDcZD", "149577529000023", "Kroma_IO"],
               ["742696043194363", "3c4845e267320278e85a8e7c6503f6b8", "EAAKjemXUpZCsBAORWJCuKfeQrE0bjNfc9jSaLFazEWfyr2eZCcNMouXRdLlPwaj122YIh5HMaAwy2K5CpEZCKOK9CH4NZBdarCQUZBNL1pxDijvfWrs7011UyefHuWYDhg5pPC4fdTlXI1bRZBwXsgl4LYRSw5uJGwfUz1ZCWh9ipKj7QAyuizL4ZB3wTT6ggv4ZD", "96254400", "Taiwan"],
               ["697549350844260", "d8645ec68374e72fdb1ab8f3cbf93c42", "EAAJ6at1FN2QBACtdM8RqToTI3GXFPEt6YY8LFo4IYqVKG4cm8ZAK0teVeyIZAYBaw31RqLQ1TqwiSqxzk7WKRZCJtTuA5eE0ICmLZAuwLeCgPB9vWD0WURUZAVXsDYoKZAA1GK33735D35XPe9iC2ddkhmSEsuGi3ZC5kJoOVJ3gjhlVnrCiD8oVTl1OnZCTZAhQZD", "192395238714962", "Japan"]]

for i in range(len(ad_app_list)):
    app_id = ad_app_list[i][0]
    app_secret = ad_app_list[i][1]
    access_token = ad_app_list[i][2]
    FacebookAdsApi.init(app_id, app_secret, access_token, api_version='v9.0')
    
    me = AdAccountUser(fbid='me')
    my_accounts = list(me.get_ad_accounts())
    my_ad_accounts = [n for n in my_accounts if n["account_id"] == ad_app_list[i][3]]
    
    params={'time_range': {'since': day_before_yesterday,'until': day_before_yesterday},'level': 'campaign', 'limit': '20000', 'filtering': '[{"field":"ad.effective_status","operator":"IN","value":["ACTIVE"]}]', 'action_attribution_windows': ['1d_view', '28d_click']}
    # params={'date_preset':'yesterday','level': 'campaign', 'limit': '20000', 'filtering': '[{"field":"ad.effective_status","operator":"IN","value":["ACTIVE"]}]', 'action_attribution_windows': ['1d_view', '28d_click']}
    fields = [AdsInsights.Field.account_id,
              AdsInsights.Field.account_name,
              AdsInsights.Field.campaign_id,
              AdsInsights.Field.campaign_name,
              AdsInsights.Field.spend,
              AdsInsights.Field.impressions,
              AdsInsights.Field.clicks,
              AdsInsights.Field.inline_link_clicks,
              AdsInsights.Field.unique_actions,
              AdsInsights.Field.action_values,
              AdsInsights.Field.reach,
            ]

    for account in my_ad_accounts:
        ads = account.get_insights(params=params, fields=fields)
        print(ads)
        print(len(ads))

    if len(ads) >0 :
        campaign_list = list(ads)
        campaign_data = pd.DataFrame(campaign_list)
        USD_list = ["Taiwan", "Worldwide", "Kroma_IO"]
        JPY_list = ["Japan"]
       
        print(i)
        
        filter_purchase = take_dic_value(campaign_data, "unique_actions", "action_type", "offsite_conversion.fb_pixel_purchase", "website_purchase")
        filter_action_values = take_dic_value(campaign_data, "action_values", "action_type", "offsite_conversion.fb_pixel_purchase", "website_purchases_conversion_value")
        
        if len(filter_purchase) > 0 and len(filter_action_values) > 0:
            filter_purchase_map = filter_purchase[["campaign_name", "website_purchase"]]
            filter_action_values_map = filter_action_values[["campaign_name", "website_purchases_conversion_value"]]
            campaign_report = campaign_data.merge(filter_purchase_map, on = 'campaign_name',how = 'left')
            campaign_report = campaign_report.merge(filter_action_values_map, on = 'campaign_name',how = 'left')
            campaign_report = campaign_report.fillna(0)
            campaign_report_data = campaign_report[["campaign_name", "date_stop", "impressions", "inline_link_clicks", 
                                                    "spend", "website_purchase", "website_purchases_conversion_value"]]

            campaign_report_data = change_types(campaign_report_data)
            
            if ad_app_list[i][4] == "France":
                campaign_report_data[["split_col1", "split_col2"]] = campaign_report_data['campaign_name'].str.split('-', expand = True,n=1)
                campaign_report_data["market"] = campaign_report_data["split_col1"].apply(France_region_determination)        
         
            else:
                campaign_report_data["market"] = ad_app_list[i][4]
            
            campaign_report_data_sum = campaign_report_data.groupby(["market", "date_stop"])[["impressions", "spend", "inline_link_clicks", "website_purchase", "website_purchases_conversion_value"]].sum()
            
            campaign_report_data_sum["CTR"] = campaign_report_data_sum["inline_link_clicks"] / campaign_report_data_sum["impressions"]
            campaign_report_data_sum["CVR"] = campaign_report_data_sum["website_purchase"] / campaign_report_data_sum["inline_link_clicks"]
            campaign_report_data_sum["ROAS"] = campaign_report_data_sum["website_purchases_conversion_value"] / campaign_report_data_sum["spend"]
            campaign_report_data_sum["CPC"] = campaign_report_data_sum["inline_link_clicks"] / campaign_report_data_sum["spend"]
            
            if ad_app_list[i][4] in USD_list:
                campaign_report_data_sum["currency"] = "USD"
            elif ad_app_list[i][4] in JPY_list:
                campaign_report_data_sum["currency"] = "JPY"
            else:
                campaign_report_data_sum["currency"] = "EUR"

            campaign_report_data_sum["Platform"] = "Facebook"

            campaign_report_data_sum["Date"] = day_before_yesterday
             
            campaign_report_data_sum = campaign_report_data_sum.reset_index()
            campaign_report_data_sum = campaign_report_data_sum[["Platform", "Date", "market", "impressions", "inline_link_clicks", 
                                                                 "spend", "website_purchase", "website_purchases_conversion_value", "CTR",
                                                                 "CVR", "ROAS", "CPC", "currency"]]
            campaign_report_data_sum.columns = ["Platform", "Date", "Market", "Impressions", "Clicks", "Costs", "Purchases", 
                                                "Conversion_value", "CTR", "CVR", "ROAS", "CPC", "Currency"]
            
            to_csv_append(campaign_report_data_sum, "FB_ads_insight_report.csv")

        else:
            
            campaign_report = campaign_data.fillna(0)
            campaign_report_data = campaign_report[["campaign_name", "date_stop", "impressions", "inline_link_clicks", "spend"]]
            
            convert_dict = {"impressions" : int, "inline_link_clicks" : int, "spend": float}
            campaign_report_data = campaign_report_data.astype(convert_dict)
            
            if ad_app_list[i][4] ==  "France":
                campaign_report_data[["split_col1", "split_col2"]] = campaign_report_data['campaign_name'].str.split('-', expand = True,n=1)
                campaign_report_data["market"] = campaign_report_data["split_col1"].apply(France_region_determination)
            else:
                campaign_report_data["market"] = ad_app_list[i][4]
                
            campaign_report_data_sum = campaign_report_data.groupby(["market", "date_stop"])[["impressions", "spend", "inline_link_clicks"]].sum()
            

            campaign_report_data_sum["website_purchase"] = 0

            campaign_report_data_sum["website_purchases_conversion_value"] = 0

            campaign_report_data_sum["CTR"] = campaign_report_data_sum["inline_link_clicks"] / campaign_report_data_sum["impressions"]
            campaign_report_data_sum["CVR"] = campaign_report_data_sum["website_purchase"] / campaign_report_data_sum["inline_link_clicks"]
            campaign_report_data_sum["ROAS"] = campaign_report_data_sum["website_purchases_conversion_value"] / campaign_report_data_sum["spend"]
            campaign_report_data_sum["CPC"] = campaign_report_data_sum["inline_link_clicks"] / campaign_report_data_sum["spend"]
            

            if ad_app_list[i][4] in USD_list:
                campaign_report_data_sum["currency"] = "USD"
            elif ad_app_list[i][4] in JPY_list:
                campaign_report_data_sum["currency"] = "JPY"
            else:
                campaign_report_data_sum["currency"] = "EUR"

            campaign_report_data_sum["Platform"] = "Facebook"

            campaign_report_data_sum["Date"] = day_before_yesterday

            campaign_report_data_sum = campaign_report_data_sum.reset_index()
            campaign_report_data_sum = campaign_report_data_sum[["Platform", "Date", "market", "impressions", "inline_link_clicks", 
                                                                 "spend", "website_purchase", "website_purchases_conversion_value", "CTR",
                                                                 "CVR", "ROAS", "CPC", "currency"]]
            
            campaign_report_data_sum.columns = ["Platform", "Date", "Market", "Impressions", "Clicks", "Costs", "Purchases", 
                                                "Conversion_value", "CTR", "CVR", "ROAS", "CPC", "Currency"]
            
            to_csv_append(campaign_report_data_sum, "FB_ads_insight_report.csv")

    else:
        print("No Data")

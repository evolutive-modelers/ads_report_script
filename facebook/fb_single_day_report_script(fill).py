import pandas as pd
import os
import datetime
from pandas.io.json import json_normalize
from facebook_business.api import FacebookAdsApi
from facebook_business.adobjects.adaccount import AdAccount
from facebook_business.adobjects.adaccountuser import AdAccountUser
from facebook_business.adobjects.campaign import Campaign as AdCampaign
from facebook_business.adobjects.adsinsights import AdsInsights

def take_dic_value(df, process_col, process_key, filter_string, new_col_name):
    explode_df = df.explode(process_col).reset_index()
    explode_df = explode_df.dropna()
    filter_df = explode_df[explode_df[process_col].apply(lambda x: x[process_key] == filter_string)]
    filter_df = filter_df.reset_index()
    if len(filter_df) > 0:
        new_df = json_normalize(filter_df[process_col]).filter(like='value')
        new_df.columns = new_df.columns.str.replace('.value','')
        new_df.columns = [new_col_name]
        filter_df = filter_df.join(new_df) 
    else:
        filter_df
        
    return filter_df

def filter_column_str_contain(df, column, filter_str):
    filter_df = df[df[column].str.contains(filter_str)]
    return filter_df

def change_types(df):
    convert_dict = {"impressions" : int, "reach" : int, "inline_link_clicks" : int, 
                    "spend": float, "website_purchase" : int, "website_purchases_conversion_value" : float}
    df = df.astype(convert_dict)
    return df

def sum_groupby_and_newCol(df, groupby_columns, new_columns, inside):
    df_report = df.groupby([groupby_columns]).sum().reset_index()
    df_report[new_columns] = inside
    return df_report

def sum_groupby_and_newCol_France(df, groupby_column_1, groupby_column_2, new_columns, inside):
    df_report = df.groupby([groupby_column_1, groupby_column_2]).sum().reset_index()
    df_report[new_columns] = inside
    return df_report

def to_csv_append(df, file_name):
    filepath  = ("C:/Users/miles/OneDrive - 愛進化科技股份有限公司 (2)/Data/FB/" + file_name)
    if not os.path.isfile(filepath):
        df.to_csv(filepath, header='column_names', index=False)
    else:
        df.to_csv(filepath, mode='a', header=False, index=False)

def France_region_determination(x):
    if str(x) == "FR ":
        return "Franch"
    elif str(x) == "DE ":
        return "Germany"
    else:
        return "Spain"
        
def currency(region, region_list, df):
    if region in region_list:
        df["currency"] = "USD"
    else:
        df["currency"] = "EUR"
    return df

ad_app_list = [["877192079355060", "37df2da5587093f7f0e2f61a49976e4e", "EAAMdzTJKLLQBABk5mYhizTBOZAsX1EtmJZBbz5qRrdGb66itw0vDpuNEOWljEbMyOuTwJPPkHMh0XMESQbIQzAHnTHDdzyFG2wZCh7twoXEZAG9LXZBYZC4noR532zZAFRTzotd2NwP0w76559mxaTFqpMOdjVjJoaDidagvs14WbCyOnEtpQQQ3ABR8VeIASD3dwftb7kZA3eiGzlBE1jH9", "28945450", "worldwide"],
               ["316009306278479", "02f20516abc8c08f32da5ab895f47e2d", "EAAEfaKZAySk8BAPx87AuaYVTWDBHh88zM0mNyVNTVIxfEm14JJaSks4nT5YDedmVS3kK3junexBmTHmg8uMMMHKJqlVPCtnUCLsZAWCHhaYihG3ixBJZBZCVqpMXuYZAhiwtLsv63dcZAZCY9HNUcozAbolzRtofNjZAIT3gSrtxNAWtT0pPpwwctZAxMmPk1tR8ki7kndezK7B88wUgia58M", "2956186747755778", "france"],
               ["640584500206952", "1c1f1ee995ef9b23d1ce7761f05b0366", "EAAJGm7OVXWgBAFlOPT8Tq2PtmAoA6CFzOEtstfy5Sx0X4RLqdWRtZA23OUaZAFHklEnN2kZB6jJCoEQnZARBmKrD2K7xlo1A9a8ZAiIEhAN7Qg3UeWgC13mgBXDX9EanOifHI1dlEPw4AWEF7LnuPIBWZCpTQJCZAaONTJSr3GZC0v8ymXHiNSMq4NI80pHDNJ2xs0we2vAHzglwcLEqyZCYd", "2357460584492701", "kroma_fr"],
               ["307440447198665", "84075cee4c1338301478de5d25c677f1", "EAAEXnY6lFckBAAqZCSKzLqEWlsJ9ZA06EEi08ZA6E2ZAZCpUwp5y5pOPkYZA9fwxZCxBRA2t7VwtPgrruvSRIp0QmBPFff9fd7rcPxzvqUlFV8ZBRszj19aVGtNXYlIXyhpNl6VyK2HAnUZBjg5SYbCAy37j5pNNmZCZCaGWlrtI1OGZByOq1ZAOus6g0o1T35MtGHrsFhFZCJtZALuO6sAz4UPxQr8", "149577529000023", "kroma_IO"]]
#              ["742696043194363", "3c4845e267320278e85a8e7c6503f6b8", "EAAKjemXUpZCsBAKDCCRZBptMesbiIzkUoUkurEEPzJftgXfT7cXiHhjUDOGZAWcJowWdzhyQQDGBER37CsNrIAZCEZBmIpblTtRIfWJsqEwYwmf1eQJQdaZBZA6FsomxQYQ2u1ZAqO2yh282kaQH1DqmjxQ7s52iUuKKKR90ZCD0fWneZCBZBUeoqgIS0ttkmBR0DkZD", "96254400", "Taiwan"]


k = "2020-11-10"
year = datetime.date.today().year

for i in range(len(ad_app_list)):
    app_id = ad_app_list[i][0]
    app_secret = ad_app_list[i][1]
    access_token = ad_app_list[i][2]
    FacebookAdsApi.init(app_id, app_secret, access_token)
    
    me = AdAccountUser(fbid='me')
    my_accounts = list(me.get_ad_accounts())
    my_ad_accounts = [n for n in my_accounts if n["account_id"] == ad_app_list[i][3]]
    
    params={'time_range': {'since': k,'until': k},'level': 'campaign', 'limit': '20000', 'filtering': '[{"field":"ad.effective_status","operator":"IN","value":["ACTIVE"]}]', 'action_attribution_windows': ['1d_view', '28d_click']}
    fields = [AdsInsights.Field.account_id,
              AdsInsights.Field.account_name,
              AdsInsights.Field.campaign_id,
              AdsInsights.Field.campaign_name,
              AdsInsights.Field.spend,
              AdsInsights.Field.impressions,
              AdsInsights.Field.clicks,
              AdsInsights.Field.inline_link_clicks,
              AdsInsights.Field.unique_actions,
              AdsInsights.Field.action_values,
              AdsInsights.Field.reach,
            ]

    for account in my_ad_accounts:
        ads = account.get_insights(params=params, fields=fields)
        print(ads)
        print(len(ads))

    campaign_list = list(ads)
    campaign_data = pd.DataFrame(campaign_list)
    USD_list = ["taiwan", "worldwide", "kroma_IO"]
    
    filter_purchase = take_dic_value(campaign_data, "unique_actions", "action_type", "offsite_conversion.fb_pixel_purchase", "website_purchase")
    filter_action_values = take_dic_value(campaign_data, "action_values", "action_type", "offsite_conversion.fb_pixel_purchase", "website_purchases_conversion_value")
    
    if len(filter_purchase) > 0 and len(filter_action_values) > 0:
        filter_purchase_map = filter_purchase[["campaign_name", "website_purchase"]]
        filter_action_values_map = filter_action_values[["campaign_name", "website_purchases_conversion_value"]]
        campaign_report = campaign_data.merge(filter_purchase_map, on = 'campaign_name',how = 'left')
        campaign_report = campaign_report.merge(filter_action_values_map, on = 'campaign_name',how = 'left')
        campaign_report = campaign_report.fillna(0)
        campaign_report_data = campaign_report[["campaign_name", "date_stop", "impressions", "reach", "inline_link_clicks", 
                                                "spend", "website_purchase", "website_purchases_conversion_value"]]

        Retargeting_data = filter_column_str_contain(campaign_report_data, "campaign_name", "Retargeting")
        New_Audience_data = filter_column_str_contain(campaign_report_data, "campaign_name", "New Audience")

        Retargeting_data = change_types(Retargeting_data)
        New_Audience_data = change_types(New_Audience_data)
        
        if ad_app_list[i][4] == "france":
            Retargeting_data[["split_col1", "split_col2"]] = Retargeting_data['campaign_name'].str.split('-', expand = True,n=1)
            New_Audience_data[["split_col1", "split_col2"]] = New_Audience_data['campaign_name'].str.split('-', expand = True,n=1)
            Retargeting_data["market"] = Retargeting_data["split_col1"].apply(France_region_determination)
            New_Audience_data["market"] = New_Audience_data["split_col1"].apply(France_region_determination)
            Retargeting_report = sum_groupby_and_newCol_France(Retargeting_data, "date_stop", "market", "ADs_type", "Retargeting")
            New_Audience_report = sum_groupby_and_newCol_France(New_Audience_data, "date_stop", "market", "ADs_type", "New_Audience")
            Retargeting_report["website_purchase_roas_value"] = Retargeting_report["website_purchases_conversion_value"] / Retargeting_report["spend"]
            New_Audience_report["website_purchase_roas_value"] = New_Audience_report["website_purchases_conversion_value"] / New_Audience_report["spend"]
        
        else:
            Retargeting_report = sum_groupby_and_newCol(Retargeting_data, "date_stop", "ADs_type", "Retargeting")
            New_Audience_report = sum_groupby_and_newCol(New_Audience_data, "date_stop", "ADs_type", "New_Audience")

            Retargeting_report["market"] = ad_app_list[i][4]
            New_Audience_report["market"] = ad_app_list[i][4]

            Retargeting_report["website_purchase_roas_value"] = Retargeting_report["website_purchases_conversion_value"] / Retargeting_report["spend"]
            New_Audience_report["website_purchase_roas_value"] = New_Audience_report["website_purchases_conversion_value"] / New_Audience_report["spend"]

        Retargeting_report = currency(ad_app_list[i][4], USD_list, Retargeting_report)
        New_Audience_report = currency(ad_app_list[i][4], USD_list, New_Audience_report)

        Retargeting_report["year"] = year
        New_Audience_report["year"] = year

        Retargeting_report = Retargeting_report[["ADs_type", "spend", "reach", "inline_link_clicks", "website_purchase", 
                                                 "website_purchases_conversion_value", "website_purchase_roas_value", 
                                                 "market", "currency", "date_stop", "year"]]

        # Retargeting_report = Retargeting_report[["ADs_type", "market", "currency", "date_stop", "reach", "impressions", 
        #                                          "inline_link_clicks", "spend", "website_purchase", 
        #                                          "website_purchases_conversion_value", "website_purchase_roas_value"]]

        New_Audience_report = New_Audience_report[["ADs_type", "spend", "reach", "inline_link_clicks", "website_purchase", 
                                                   "website_purchases_conversion_value", "website_purchase_roas_value", 
                                                   "market", "currency", "date_stop", "year"]]

        to_csv_append(Retargeting_report, "FB_Ads_report.csv")
        to_csv_append(New_Audience_report, "FB_Ads_report.csv")

    else:
        campaign_report = campaign_data.fillna(0)
        campaign_report_data = campaign_report[["campaign_name", "date_stop", "impressions", "reach", "inline_link_clicks", "spend"]]
        
        Retargeting_data = filter_column_str_contain(campaign_report_data, "campaign_name", "Retargeting")
        New_Audience_data = filter_column_str_contain(campaign_report_data, "campaign_name", "New Audience")
        
        convert_dict = {"impressions" : int, "reach" : int, "inline_link_clicks" : int, "spend": float}
        Retargeting_data = Retargeting_data.astype(convert_dict)
        New_Audience_data = New_Audience_data.astype(convert_dict)
        
        if ad_app_list[i][4] ==  "france":
            Retargeting_data[["split_col1", "split_col2"]] = Retargeting_data['campaign_name'].str.split('-', expand = True,n=1)
            New_Audience_data[["split_col1", "split_col2"]] = New_Audience_data['campaign_name'].str.split('-', expand = True,n=1)
            Retargeting_data["market"] = Retargeting_data["split_col1"].apply(France_region_determination)
            New_Audience_data["market"] = New_Audience_data["split_col1"].apply(France_region_determination)
            Retargeting_report = sum_groupby_and_newCol_France(Retargeting_data, "date_stop", "market", "ADs_type", "Retargeting")
            New_Audience_report = sum_groupby_and_newCol_France(New_Audience_data, "date_stop", "market", "ADs_type", "New_Audience")
        else:
            Retargeting_report = sum_groupby_and_newCol(Retargeting_data, "date_stop", "ADs_type", "Retargeting")
            New_Audience_report = sum_groupby_and_newCol(New_Audience_data, "date_stop", "ADs_type", "New_Audience")
            Retargeting_report["market"] = ad_app_list[i][4]
            New_Audience_report["market"] = ad_app_list[i][4]
    

        Retargeting_report["website_purchase"] = 0
        New_Audience_report["website_purchase"] = 0

        Retargeting_report["website_purchases_conversion_value"] = 0
        New_Audience_report["website_purchases_conversion_value"] = 0

        Retargeting_report["website_purchase_roas_value"] = 0
        New_Audience_report["website_purchase_roas_value"] = 0
        Retargeting_report = currency(ad_app_list[i][4], USD_list, Retargeting_report)
        New_Audience_report = currency(ad_app_list[i][4], USD_list, New_Audience_report)

        Retargeting_report["year"] = year
        New_Audience_report["year"] = year

        Retargeting_report = Retargeting_report[["ADs_type", "spend", "reach", "inline_link_clicks", "website_purchase", 
                                                 "website_purchases_conversion_value", "website_purchase_roas_value", 
                                                 "market", "currency", "date_stop", "year"]]
        New_Audience_report = New_Audience_report[["ADs_type", "spend", "reach", "inline_link_clicks", "website_purchase", 
                                                   "website_purchases_conversion_value", "website_purchase_roas_value", 
                                                   "market", "currency", "date_stop", "year"]]
        to_csv_append(Retargeting_report, "FB_Ads_report.csv")
        to_csv_append(New_Audience_report, "FB_Ads_report.csv")

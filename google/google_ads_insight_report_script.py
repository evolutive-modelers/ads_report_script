from googleads import adwords
import sys
import _locale
import io
import pandas as pd
import os
from datetime import date, timedelta

_locale._getdefaultlocale = (lambda *args: ['en_US', 'UTF-8'])

adwords_client = adwords.AdWordsClient.LoadFromStorage("D:/google/googleads.yaml")

account_list = [["172-595-6970", "Taiwan"],
                ["778-353-1847", "France"], 
                ["182-592-6970", "Worldwide"],
                ["941-151-9288", "Germany"],
                ["925-796-9680", "Spain"],
                ["916-616-0107", "Japan"]]

day_before_yesterday = date.today() - timedelta(days=2)
day_before_yesterday = day_before_yesterday.strftime('%Y%m%d')

def main(account):
    output_file = 'D:/google/report_download.csv'
    output = io.StringIO()
    adwords_client.SetClientCustomerId(account)
    report_downloader = adwords_client.GetReportDownloader(version='v201809')
    report_query = (adwords.ReportQueryBuilder()
                   .Select('Date', 'CampaignName', 'Impressions', 'Clicks', 'Engagements', 'Cost', 'Conversions', 'ConversionValue')
                   .From('CRITERIA_PERFORMANCE_REPORT')
                   .Where('Status').In('ENABLED')
                   .During(start_date=day_before_yesterday, end_date=day_before_yesterday)
                   .Build())

    with open(output_file, 'w'):
        sys.stdout = open(output_file, 'w')
        report_downloader.DownloadReportWithAwql(
            report_query, 'CSV', sys.stdout, skip_report_header=True,
            skip_column_header=False, skip_report_summary=True,
            include_zero_impressions=True)

    ads_df = pd.read_csv(output_file, header=0, error_bad_lines = False)
    return ads_df


def to_csv_append(df, file_name):
    filepath  = ("C:/Users/miles/OneDrive - 愛進化科技股份有限公司 (2)/Data/Ads_platform_report/" + file_name)
    if not os.path.isfile(filepath):
        df.to_csv(filepath, header='column_names', index=False)
    else:
        df.to_csv(filepath, mode='a', header=False, index=False)

for i in range(len(account_list)):
    ads_df = main(account_list[i][0])
    ads_df["Total_Clicks"] = ads_df["Clicks"] + ads_df["Engagements"]
    ad_sum = ads_df.groupby(['Day'])[["Impressions", "Total_Clicks", "Cost", "Conversions", "Total conv. value"]].sum()
    ad_sum["Cost"] = ad_sum["Cost"] / 1000000
    ad_sum["CTR"] = ad_sum["Total_Clicks"] / ad_sum["Impressions"]
    ad_sum["CVR"] = ad_sum["Conversions"] / ad_sum["Total_Clicks"]
    ad_sum["ROAS"] = ad_sum["Total conv. value"] / ad_sum["Cost"]
    ad_sum["CPC"] = ad_sum["Total_Clicks"] / ad_sum["Cost"]
    ad_sum["market"] = account_list[i][1]
    google_ad_report = ad_sum.fillna(0)

    google_ad_report["Platform"] = "Google"
    
    USD_list = ["Germany", "Spain", "Japan"]
    if account_list[i][1] in USD_list:
        google_ad_report["currency"] = "USD"
    else:
        google_ad_report["currency"] = "TWD"

    google_ad_report = google_ad_report[google_ad_report["CPC"] != 0]

    google_ad_report = google_ad_report.astype({"Conversions" : int})
    google_ad_report = google_ad_report.reset_index()
    google_ad_report = google_ad_report[["Platform", "Day", "market", "Impressions", "Total_Clicks", "Cost", "Conversions",
                                         "Total conv. value", "CTR", "CVR", "ROAS", "CPC", "currency"]]
    google_ad_report.columns = ["Platform", "Date", "Market", "Impressions", "Total_Clicks", "Costs", "Purchases", 
                                "Conversion_value", "CTR", "CVR", "ROAS", "CPC", "Currency"]
    to_csv_append(google_ad_report, "google_ad_insight_report.csv")